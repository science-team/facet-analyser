Source: facet-analyser
Section: science
Priority: optional
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders: Roland Mas <lolando@debian.org>, Frédéric-Emmanuel Picca <picca@debian.org>
Build-Depends:
 cmake,
 debhelper-compat (= 13),
 libdouble-conversion-dev,
 libfftw3-dev,
 libgdcm-tools,
 libinsighttoolkit5-dev,
 liblz4-dev,
 libopengl-dev,
 libqt5opengl5-dev,
 libqt5svg5-dev,
 libutfcpp-dev,
 libvtkgdcm-dev,
 libx11-dev,
 paraview-dev,
 python3-paraview,
 python3-vtkgdcm,
 qtbase5-dev,
 qttools5-dev,
 qtxmlpatterns5-dev-tools,
 xalan,
 xvfb <!nocheck>,
 xauth <!nocheck>,
 libogg-dev,
 libtheora-dev,
 libavcodec-dev,
 libavdevice-dev,
 libavfilter-dev,
 libavformat-dev,
 libavutil-dev,
 libswresample-dev,
 libswscale-dev,
 libgl2ps-dev,
 libgdal-dev,
 libopenmpi-dev,
 libglew-dev,
 libfreetype-dev
Standards-Version: 4.6.0
Homepage: https://github.com/picca/FacetAnalyser
Vcs-Git: https://salsa.debian.org/science-team/facet-analyser.git
Vcs-Browser: https://salsa.debian.org/science-team/facet-analyser
Rules-Requires-Root: no

Package: facet-analyser
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: ParaView plugin for facet detection and angles measurement
 The presented ParaView plugin allows easy access to the algorithm
 described in Ref 1. It enables analysis of faceted objects that
 exhibit distortions in their digital representation, e.g. due to
 tomographic reconstruction artifacts. The contributed functionality
 can also be used outside ParaView in e.g. command-line programs. The
 code, data, a test and an example program are included.
 .
 Ref 1: Roman Grothausmann, Sebastian Fiechter, Richard Beare, Gaëtan
 Lehmann, Holger Kropf, Goarke Sanjeeviah Vinod Kumar, Ingo Manke, and
 John Banhart. Automated quantitative 3D analysis of faceting of
 particles in tomographic datasets. Ultramicroscopy, 122(0):65 – 75,
 2012. ISSN 0304- 3991. doi: 10.1016/j.ultramic.2012.07.024.
